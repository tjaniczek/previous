import React from 'react';
import { get, mapValues } from 'lodash';
import uuid from 'uuid';

import features from './features';

require('./scss/EmptyComponent.scss');

const isProduction = () => FEATURES_ENV === 'production'; // eslint-disable-line no-undef

const throwIfUsed = (fragment, passedElement) => {
    const name = passedElement.name;
    const fragmentName = fragment.definitions ? fragment.definitions[0].name.value : '';
    const productionMessage = 'Internal error';
    const developmentMessage = `Trying to access fragment ${fragmentName} from disabled component: ${name}`;

    try {
        return new Proxy(fragment, {
            get: (fragment, () => {
                throw new Error(isProduction() ? productionMessage : developmentMessage);
            }),
        });
    } catch (e) {
        throw new Error(productionMessage);
    }
};

const disableFragments = passedElement => mapValues(
    passedElement.fragments,
    fragment => throwIfUsed(fragment, passedElement),
);

const getEmptyComponentDefinition = (passedElement, featureKey, displayPlaceholder) => {
    class EmptyComponent extends React.Component {
        render() {
            const placeholder = (
                <div className="empty-component">
                    <strong>{featureKey}</strong> is under development
                </div>
            );

            return displayPlaceholder ? placeholder : null;
        }
    }

    EmptyComponent.fragments = disableFragments(passedElement);

    return EmptyComponent;
};

const isComponent = (passed) => {
    const isHTMLNode = typeof passed.type === 'string';
    const isReactNode = typeof passed.type === 'function' && typeof passed.type.name === 'string';

    return isHTMLNode || isReactNode;
};

const getEmptyComponent = (passedElement, featureName, displayPlaceholder) => {
    const Empty = getEmptyComponentDefinition(passedElement, featureName, displayPlaceholder);
    return <Empty key={uuid()} />;
};

const getEmptyObject = (passedElement, featureKey, displayPlaceholder) => {
    if (isComponent(passedElement)) {
        return getEmptyComponent(passedElement, featureKey, displayPlaceholder);
    }

    return undefined;
};

const getEmptyString = (featureName, displayPlaceholder) => (
    displayPlaceholder ? `[${featureName} is under development]` : ''
);

const hideElement = (passedElement, featureKey, placeholder) => {
    const displayPlaceholder = isProduction() ? false : placeholder;
    const elementType = passedElement && typeof passedElement;

    switch (elementType) {
    case 'undefined':
        // For simple if statements
        return false;
    case 'function':
        // React component definitions
        return getEmptyComponentDefinition(passedElement, featureKey, displayPlaceholder);
    case 'object':
        // React components and GraphQL queries
        return getEmptyObject(passedElement, featureKey, displayPlaceholder);
    case 'string':
        // Strings inside JSX
        return getEmptyString(featureKey, displayPlaceholder);
    default:
        throw new Error(isProduction() ? 'Internal error' : 'Invalid element passed to the feature toggle');
    }
};

const getSetting = featureKey => get(features, [featureKey, FEATURES_ENV]); // eslint-disable-line no-undef

const isEnabled = featureKey => Boolean(getSetting(featureKey));

const featureToggle = (featureKey, placeholder = false) => passedElement => (
    isEnabled(featureKey) ? passedElement : hideElement(passedElement, featureKey, placeholder)
);

export { isEnabled };
export default featureToggle;
