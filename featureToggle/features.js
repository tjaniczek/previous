export default {
    auth: {
        development: true,
        staging: AUTH_FEATURE_ENABLED,
        qa: AUTH_FEATURE_ENABLED,
        production: AUTH_FEATURE_ENABLED,
    },
    authDebugger: { 
        development: true, 
        staging: false, 
        qa: false, 
        production: false 
    },
    forgotPassword: { 
        development: true, 
        staging: false, 
        qa: false, 
        production: false 
    },
    sentry: {
        development: RAVEN_PUBLIC_DSN,
        staging: RAVEN_PUBLIC_DSN,
        qa: RAVEN_PUBLIC_DSN,
        production: RAVEN_PUBLIC_DSN,
    },
    googleAnalytics: {
        development: GOOGLE_ANALYTICS,
        staging: GOOGLE_ANALYTICS,
        qa: GOOGLE_ANALYTICS,
        production: GOOGLE_ANALYTICS,
    },
};

