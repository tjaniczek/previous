import { isEnabled } from '../';

jest.mock('../../../features', () => ({
    'in-development': { development: true },
    'in-development-full': { development: true, staging: false, qa: false, production: false },
    'in-staging': { development: false, staging: true, qa: false, production: false },
    'in-qa': { development: false, staging: false, qa: true, production: false },
    'prod-only': { production: true },
    all: { development: true, staging: true, qa: true, production: true },
}));

describe('isEnabled helper', () => {
    describe('in production', () => {
        beforeAll(() => {
            global.FEATURES_ENV = 'production';
        });

        test('should return false for feature in development', () => {
            expect(isEnabled('in-development')).toBe(false);
            expect(isEnabled('in-development-full')).toBe(false);
        });

        test('should return false for feature in staging', () => {
            expect(isEnabled('in-staging')).toBe(false);
        });

        test('should return false for feature in qa', () => {
            expect(isEnabled('in-qa')).toBe(false);
        });

        test('should allow features in production only', () => {
            expect(isEnabled('prod-only')).toBe(true);
        });

        test('should allow features available everywhere', () => {
            expect(isEnabled('all')).toBe(true);
        });
    });

    describe('in staging or review apps', () => {
        beforeAll(() => {
            global.FEATURES_ENV = 'staging';
        });

        test('should return false for feature in development', () => {
            expect(isEnabled('in-development')).toBe(false);
            expect(isEnabled('in-development-full')).toBe(false);
        });

        test('should return false for feature in development', () => {
            expect(isEnabled('in-development')).toBe(false);
            expect(isEnabled('in-development-full')).toBe(false);
        });

        test('should return true for feature in staging', () => {
            expect(isEnabled('in-staging')).toBe(true);
        });

        test('should return false for feature in qa', () => {
            expect(isEnabled('in-qa')).toBe(false);
        });

        test('should allow features in production only', () => {
            expect(isEnabled('prod-only')).toBe(false);
        });

        test('should allow features available everywhere', () => {
            expect(isEnabled('all')).toBe(true);
        });
    });

    describe('in qa', () => {
        beforeAll(() => {
            global.FEATURES_ENV = 'qa';
        });

        test('should return false for feature in development', () => {
            expect(isEnabled('in-development')).toBe(false);
            expect(isEnabled('in-development-full')).toBe(false);
        });

        test('should return false for feature in staging', () => {
            expect(isEnabled('in-staging')).toBe(false);
        });

        test('should return true for feature in qa', () => {
            expect(isEnabled('in-qa')).toBe(true);
        });

        test('should allow features in production only', () => {
            expect(isEnabled('prod-only')).toBe(false);
        });

        test('should allow features available everywhere', () => {
            expect(isEnabled('all')).toBe(true);
        });
    });

    describe('in development', () => {
        beforeAll(() => {
            global.FEATURES_ENV = 'development';
        });

        test('should return true for feature in development', () => {
            expect(isEnabled('in-development')).toBe(true);
            expect(isEnabled('in-development-full')).toBe(true);
        });

        test('should return false for feature in staging', () => {
            expect(isEnabled('in-staging')).toBe(false);
        });

        test('should return false for feature in qa', () => {
            expect(isEnabled('in-qa')).toBe(false);
        });
        test('should allow features in production only', () => {
            expect(isEnabled('prod-only')).toBe(false);
        });
        test('should allow features available everywhere', () => {
            expect(isEnabled('all')).toBe(true);
        });
    });
});
