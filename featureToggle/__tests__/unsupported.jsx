import featureToggle from '../';

jest.mock('../../../features', () => ({
    'in-development': { development: true },
}));

describe('featureToggle', () => {
    describe('when passing an unsupported value', () => {
        test('should throw generic error on production environments', () => {
            global.FEATURES_ENV = 'production';
            expect(() => featureToggle('in-development')(null))
                .toThrow('Internal error');
        });

        test('should throw detailed error on development environments', () => {
            global.FEATURES_ENV = 'qa';
            expect(() => featureToggle('in-development')(null))
                .toThrow('Invalid element passed to the feature toggle');
        });
    });
});
