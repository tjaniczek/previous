import React from 'react';
import { shallow } from 'enzyme';

import featureToggle from '../';

jest.mock('../../../features', () => ({
    'in-development': { development: true },
    'in-development-full': { development: true, staging: false, qa: false, production: false },
    'in-staging': { development: false, staging: true, qa: false, production: false },
    'in-qa': { development: false, staging: false, qa: true, production: false },
    'prod-only': { production: true },
    'all-features': { development: true, staging: true, qa: true, production: true },
}));

const useVariable = variable => variable + 1;

describe('featureToggle', () => {
    beforeEach(() => {
        global.FEATURES_ENV = 'production';
    });

    const fragmentMock = { definitions: [{ name: { value: 'TestFragment' } }] };

    describe('when passing a component definition', () => {
        describe('when component should be available', () => {
            it('should return definition', () => {
                class MyComponent {
                    render() {
                        return <div>Hello</div>;
                    }
                }

                const HOC = featureToggle('all-features')(MyComponent);
                expect(HOC.name).toBe('MyComponent');

                const component = shallow(<div><HOC /></div>);
                expect(component.find(MyComponent)).toHaveLength(1);
            });

            it('should respond to environment variable', () => {
                class MyComponent {
                    render() {
                        return <div>Hello</div>;
                    }
                }

                global.FEATURES_ENV = 'development';
                const HOC = featureToggle('in-development')(MyComponent);
                expect(HOC.name).toBe('MyComponent');

                const component = shallow(<div><HOC /></div>);
                expect(component.find(MyComponent)).toHaveLength(1);
            });

            it('should keep fragments, propTypes and  defaultProps', () => {
                class MyComponent {
                    render() {
                        return <div>Hello</div>;
                    }
                }

                MyComponent.fragments = { fragment: 1 };
                MyComponent.propTypes = { prop: 2 };
                MyComponent.defaultProps = { default: 3 };

                const HOC = featureToggle('all-features')(MyComponent);

                expect(HOC.fragments).toEqual(MyComponent.fragments);
                expect(HOC.propTypes).toEqual(MyComponent.propTypes);
                expect(HOC.defaultProps).toEqual(MyComponent.defaultProps);
            });
        });

        describe('when component should not be available', () => {
            it('should return definition of an empty component', () => {
                class MyComponent {
                    render() {
                        return <div>Hello</div>;
                    }
                }

                const HOC = featureToggle('in-development')(MyComponent);
                expect(HOC.name).toBe('EmptyComponent');

                const component = shallow(<div><HOC /></div>);
                expect(component.find(MyComponent)).toHaveLength(0);
            });

            it('should not pass propTypes or defaultProps to an empty component', () => {
                class MyComponent {
                    render() {
                        return <div>Hello</div>;
                    }
                }

                MyComponent.propTypes = { prop: 2 };
                MyComponent.defaultProps = { default: 3 };

                const HOC = featureToggle('in-development')(MyComponent);

                expect(HOC.propTypes).toBeUndefined();
                expect(HOC.defaultProps).toBeUndefined();
            });

            it('should throw generic error on accessing fragments of a disabled component', () => {
                class MyComponent {
                    render() {
                        return <div>Hello</div>;
                    }
                }

                MyComponent.fragments = { fragment: fragmentMock };
                const HOC = featureToggle('in-development')(MyComponent);

                expect(() => useVariable(HOC.fragments.fragment)).toThrowError('Internal error');
            });

            describe('on development environments', () => {
                it('should be able to notify user that the component is unavailable', () => {
                    global.FEATURES_ENV = 'development';

                    class MyComponent {
                        render() {
                            return <div>Hello</div>;
                        }
                    }

                    const HOC = featureToggle('in-qa', true)(MyComponent);

                    const component = shallow(<div><HOC /></div>);
                    expect(component.find('EmptyComponent').dive().text()).toEqual('in-qa is under development');
                });
                it('should not show the unavailability notification bu default', () => {
                    global.FEATURES_ENV = 'development';

                    class MyComponent {
                        render() {
                            return <div>Hello</div>;
                        }
                    }

                    const HOC = featureToggle('in-qa')(MyComponent);

                    const component = shallow(<div><HOC /></div>);
                    expect(component.find('EmptyComponent').dive().html()).toEqual(null);
                });
            });

            describe('on production environments', () => {
                it('should never show the unavailability notification', () => {
                    global.FEATURES_ENV = 'production';

                    class MyComponent {
                        render() {
                            return <div>Hello</div>;
                        }
                    }

                    const HOC = featureToggle('in-qa', true)(MyComponent);

                    const component = shallow(<div><HOC /></div>);
                    expect(component.find('EmptyComponent').dive().html()).toEqual(null);
                });
            });
        });
    });
});
