import React from 'react';
import { shallow } from 'enzyme';

import featureToggle from '../';

jest.mock('../../../features', () => ({
    'in-development': { development: true },
    'in-development-full': { development: true, staging: false, qa: false, production: false },
    'in-staging': { development: false, staging: true, qa: false, production: false },
    'in-qa': { development: false, staging: false, qa: true, production: false },
    'prod-only': { production: true },
    'all-features': { development: true, staging: true, qa: true, production: true },
}));

describe('featureToggle', () => {
    beforeEach(() => {
        global.FEATURES_ENV = 'production';
    });

    const MyComponent = () => <div>Hello</div>;

    describe('when rendering a component', () => {
        describe('when component should be available', () => {
            it('should return component', () => {
                const component = shallow(<div>{featureToggle('all-features')(<MyComponent />)}</div>);
                expect(component.find(MyComponent)).toHaveLength(1);
            });

            it('should respond to environment variable', () => {
                global.FEATURES_ENV = 'development';
                const component = shallow(<div>{featureToggle('in-development')(<MyComponent />)}</div>);
                expect(component.find(MyComponent)).toHaveLength(1);
            });
        });

        describe('when component should not be available', () => {
            it('should return definition of an empty component', () => {
                const component = shallow(<div>{featureToggle('in-development')(<MyComponent />)}</div>);
                expect(component.find(MyComponent)).toHaveLength(0);
            });

            describe('on development environments', () => {
                it('should be able to notify user that the component is unavailable', () => {
                    global.FEATURES_ENV = 'development';
                    const component = shallow(<div>{featureToggle('in-staging', true)(<MyComponent />)}</div>);
                    expect(component.find('EmptyComponent').dive().text()).toEqual('in-staging is under development');
                });

                it('should not show the unavailability notification by default', () => {
                    global.FEATURES_ENV = 'development';
                    const component = shallow(<div>{featureToggle('in-staging')(<MyComponent />)}</div>);
                    expect(component.find('EmptyComponent').dive().text()).toEqual('');
                });
            });

            describe('on production environments', () => {
                it('should never show the unavailability notification', () => {
                    global.FEATURES_ENV = 'production';
                    const component = shallow(<div>{featureToggle('in-staging', true)(<MyComponent />)}</div>);
                    expect(component.find('EmptyComponent').dive().html()).toEqual(null);
                });
            });
        });
    });
});
