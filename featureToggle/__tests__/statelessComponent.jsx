import React from 'react';
import { shallow } from 'enzyme';

import featureToggle from '../';

jest.mock('../../../features', () => ({
    'in-development': { development: true },
    'in-development-full': { development: true, staging: false, qa: false, production: false },
    'in-staging': { development: false, staging: true, qa: false, production: false },
    'in-qa': { development: false, staging: false, qa: true, production: false },
    'prod-only': { production: true },
    'all-features': { development: true, staging: true, qa: true, production: true },
}));

const useVariable = variable => variable + 1;

describe('featureToggle', () => {
    beforeEach(() => {
        global.FEATURES_ENV = 'production';
    });

    const fragmentMock = { definitions: [{ name: { value: 'TestFragment' } }] };

    describe('when passing a stateless component definition', () => {
        describe('when component should be available', () => {
            it('should return definition', () => {
                const MyStatelessComponent = () => <div>Hello</div>;

                const HOC = featureToggle('all-features')(MyStatelessComponent);
                expect(HOC.name).toBe('MyStatelessComponent');

                const component = shallow(<div><HOC /></div>);
                expect(component.find(MyStatelessComponent)).toHaveLength(1);
            });

            it('should respond to environment variable', () => {
                const MyStatelessComponent = () => <div>Hello</div>;

                global.FEATURES_ENV = 'qa';
                const HOC = featureToggle('in-qa')(MyStatelessComponent);
                expect(HOC.name).toBe('MyStatelessComponent');

                const component = shallow(<div><HOC /></div>);
                expect(component.find(MyStatelessComponent)).toHaveLength(1);
            });


            it('should keep fragments, propTypes and  defaultProps', () => {
                const MyStatelessComponent = () => <div>Hello</div>;

                MyStatelessComponent.fragments = { fragment: 1 };
                MyStatelessComponent.propTypes = { prop: 2 };
                MyStatelessComponent.defaultProps = { default: 3 };

                const HOC = featureToggle('all-features')(MyStatelessComponent);

                expect(HOC.fragments).toEqual(MyStatelessComponent.fragments);
                expect(HOC.propTypes).toEqual(MyStatelessComponent.propTypes);
                expect(HOC.defaultProps).toEqual(MyStatelessComponent.defaultProps);
            });
        });

        describe('when component should not be available', () => {
            it('should return definition of an empty component', () => {
                const MyStatelessComponent = () => <div>Hello</div>;

                const HOC = featureToggle('in-qa')(MyStatelessComponent);
                expect(HOC.name).toBe('EmptyComponent');

                const component = shallow(<div><HOC /></div>);
                expect(component.find(MyStatelessComponent)).toHaveLength(0);
            });

            it('should not pass propTypes or defaultProps to an empty component', () => {
                const MyStatelessComponent = () => <div>Hello</div>;

                MyStatelessComponent.propTypes = { prop: 2 };
                MyStatelessComponent.defaultProps = { default: 3 };

                const HOC = featureToggle('in-development')(MyStatelessComponent);

                expect(HOC.propTypes).toBeUndefined();
                expect(HOC.defaultProps).toBeUndefined();
            });

            it('should throw generic error on accessing fragments of a disabled component', () => {
                const MyStatelessComponent = () => <div>Hello</div>;

                MyStatelessComponent.fragments = { fragment: fragmentMock };
                const HOC = featureToggle('in-development')(MyStatelessComponent);

                expect(() => useVariable(HOC.fragments.fragment)).toThrowError('Internal error');
            });

            describe('on development environments', () => {
                it('should be able to notify user that the component is unavailable', () => {
                    global.FEATURES_ENV = 'development';

                    const MyStatelessComponent = () => <div>Hello</div>;

                    const HOC = featureToggle('in-qa', true)(MyStatelessComponent);

                    const component = shallow(<div><HOC /></div>);
                    expect(component.find('EmptyComponent').dive().text()).toEqual('in-qa is under development');
                });

                it('should not show the unavailability notification by default', () => {
                    global.FEATURES_ENV = 'development';

                    const MyStatelessComponent = () => <div>Hello</div>;

                    const HOC = featureToggle('in-qa')(MyStatelessComponent);

                    const component = shallow(<div><HOC /></div>);
                    expect(component.find('EmptyComponent').dive().html()).toEqual(null);
                });
            });

            describe('on production environments', () => {
                it('should never show the unavailability notification', () => {
                    global.FEATURES_ENV = 'production';

                    const MyStatelessComponent = () => <div>Hello</div>;

                    const HOC = featureToggle('in-qa', true)(MyStatelessComponent);

                    const component = shallow(<div><HOC /></div>);
                    expect(component.find('EmptyComponent').dive().html()).toEqual(null);
                });
            });
        });
    });
});
