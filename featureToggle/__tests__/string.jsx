import React from 'react';
import { shallow } from 'enzyme';

import featureToggle from '../';

jest.mock('../../../features', () => ({
    'in-development': { development: true },
    'in-development-full': { development: true, staging: false, qa: false, production: false },
    'in-staging': { development: false, staging: true, qa: false, production: false },
    'in-qa': { development: false, staging: false, qa: true, production: false },
    'prod-only': { production: true },
    'all-features': { development: true, staging: true, qa: true, production: true },
}));

describe('featureToggle', () => {
    beforeEach(() => {
        global.FEATURES_ENV = 'production';
    });

    const myString = 'hello';

    describe('when rendering a string', () => {
        describe('when component should be available', () => {
            it('should return component', () => {
                const component = shallow(<div>{featureToggle('all-features')(myString)}</div>);
                expect(component.text()).toEqual(myString);
            });

            it('should respond to environment variable', () => {
                global.FEATURES_ENV = 'development';
                const component = shallow(<div>{featureToggle('in-development')(myString)}</div>);
                expect(component.text()).toEqual(myString);
            });
        });

        describe('when component should not be available', () => {
            it('should return definition of an empty component', () => {
                const component = shallow(<div>{featureToggle('in-development')(myString)}</div>);
                expect(component.text()).toEqual('');
            });

            describe('on development environments', () => {
                it('should be able to notify user that the component is unavailable', () => {
                    global.FEATURES_ENV = 'development';
                    const component = shallow(<div>{featureToggle('in-staging', true)(myString)}</div>);
                    expect(component.text()).toEqual('[in-staging is under development]');
                });

                it('should not show the unavailability notification by default', () => {
                    global.FEATURES_ENV = 'production';
                    const component = shallow(<div>{featureToggle('in-staging')(myString)}</div>);
                    expect(component.text()).toEqual('');
                });
            });

            describe('on production environments', () => {
                it('should never show the unavailability notification', () => {
                    global.FEATURES_ENV = 'production';
                    const component = shallow(<div>{featureToggle('in-staging', true)(myString)}</div>);
                    expect(component.text()).toEqual('');
                });
            });
        });
    });
});
