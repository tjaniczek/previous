import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import HeaderContainer from '.';
import setHeaderHeight from '../../actions/headerHeight';

jest.mock('./Header');

const mockStore = configureMockStore();

describe('<HeaderContainer> HOC component', () => {
    describe('by default', () => {
        test('should pass setHeaderHeight action to <Header> component', () => {
            const store = mockStore({
                setHeaderHeight: 0,
                userInfo: {},
            });

            const container = mount(
                <Provider store={store}>
                    <HeaderContainer />
                </Provider>,
            );

            const component = container.find('Header');

            expect(component.props().setHeaderHeight(1)).toEqual(setHeaderHeight(1));
        });
    });
});
