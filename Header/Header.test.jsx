import React from 'react';
import { mount, shallow } from 'enzyme';
import Header from './Header';
import HeaderSwitcher from './HeaderSwitcher';

const setHeaderHeightAction = jest.fn();

jest.mock('./HeaderSwitcher');

describe('<Header /> component', () => {
    test('should invoke action to set header height on mount', () => {
        mount(
            <Header style={{}} setHeaderHeight={setHeaderHeightAction} />,
        );

        expect(setHeaderHeightAction).toHaveBeenCalledTimes(1);
        expect(setHeaderHeightAction).toHaveBeenCalledWith(0);
    });

    test('should contain <HeaderSwitcher /> component', () => {
        const component = shallow(<Header setHeaderHeight={setHeaderHeightAction} />);
        expect(component.find(HeaderSwitcher)).toHaveLength(1);
    });
});
