import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import HeaderSwitcher from '.';

jest.mock('../HeaderLoggedIn');

const mockStore = configureMockStore();
const routerMock = { locationBeforeTransitions: { pathname: '/' } };

describe('<HeaderSwitcher> HOC component', () => {
    let component;

    describe('by default', () => {
        beforeEach(() => {
            const store = mockStore({ userInfo: {}, routing: routerMock });

            const container = mount(
                <Provider store={store}>
                    <HeaderSwitcher />
                </Provider>,
            );

            component = container.find('HeaderSwitcher');
        });

        test('should pass isLoggedIn in prop to <HeaderSwitcher /> component', () => {
            expect(component.props().isLoggedIn).toEqual(false);
        });
    });

    describe('when user logs in', () => {
        beforeEach(() => {
            const store = mockStore({ userInfo: { id: '123' }, routing: routerMock });

            const container = mount(
                <Provider store={store}>
                    <HeaderSwitcher />
                </Provider>,
            );

            component = container.find('HeaderSwitcher');
        });

        test('should pass correct isLoggedIn in prop to <HeaderSwitcher /> component', () => {
            expect(component.props().isLoggedIn).toEqual(true);
        });
    });
});
