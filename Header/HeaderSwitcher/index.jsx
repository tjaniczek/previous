import { connect } from 'react-redux';

import HeaderSwitcher from './HeaderSwitcher';

const mapStateToProps = state => ({
    isLoggedIn: Boolean(state.userInfo.id),
    isHomepage: state.routing.locationBeforeTransitions.pathname === '/',
});

export default connect(mapStateToProps)(HeaderSwitcher);
