import React from 'react';
import PropTypes from 'prop-types';

import HeaderLoggedIn from '../HeaderLoggedIn';
import HeaderLoggedOut from '../HeaderLoggedOut';
import { isEnabled } from '../../../helpers/featureToggle';


class HeaderSwitcher extends React.Component {
    shouldComponentUpdate() {
        return true;
    }

    showLoggedInHeader() {
        if (!isEnabled('auth')) {
            return !this.props.isHomepage;
        }

        // Please refactor this component after Auth launch
        return this.props.isLoggedIn;
    }

    render() {
        return this.showLoggedInHeader() ? <HeaderLoggedIn /> : <HeaderLoggedOut />;
    }
}


HeaderSwitcher.propTypes = {
    isLoggedIn: PropTypes.bool.isRequired,
    isHomepage: PropTypes.bool.isRequired,
};

export default HeaderSwitcher;
