import React from 'react';
import { shallow } from 'enzyme';

import HeaderSwitcher from './HeaderSwitcher';
import HeaderLoggedIn from '../HeaderLoggedIn';
import HeaderLoggedOut from '../HeaderLoggedOut';

jest.mock('../../../helpers/featureToggle', () => ({
    isEnabled: () => true,
}));

describe('<HeaderSwitcher /> component', () => {
    describe('When user is logged out', () => {
        test('should display <HeaderLoggedOut /> component', () => {
            const component = shallow(<HeaderSwitcher isLoggedIn={false} isHomepage={false} />);

            expect(component.find(HeaderLoggedIn)).toHaveLength(0);
            expect(component.find(HeaderLoggedOut)).toHaveLength(1);
        });
    });

    describe('When user is logged in', () => {
        test('should display <HeaderLoggedIn /> component', () => {
            const component = shallow(<HeaderSwitcher isLoggedIn isHomepage={false} />);

            expect(component.find(HeaderLoggedIn)).toHaveLength(1);
            expect(component.find(HeaderLoggedOut)).toHaveLength(0);
        });
    });
});
