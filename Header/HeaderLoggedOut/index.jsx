import React from 'react';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import ExploreUrl from '../../../apps/Explore/url';

require('./scss/HeaderLoggedOut.scss');

const HeaderLoggedOut = props => (
    <header className="sales-header" style={props.style}>
        <div className="sales-header__container">
            <div className="sales-header__logo-wrapper">
                <Link to={ExploreUrl} className="sales-header__logo">
                    <h1 className="show-for-sr">Twig Science Tools</h1>
                </Link>
            </div>
            <nav className="hide-for-print sales-header__main-menu" role="navigation">
                <ul>
                    <li><Link to="/about-us">About us</Link></li>
                    <li><Link to="/contact-us">Contact us</Link></li>
                    <li>
                        <Link to={ExploreUrl} className="sales-header__btn">
                            <span className="hide-for-small-only">Homepage
                                <span className="icon icon-arrow-right" />
                            </span>
                            <span className="icon icon-home show-for-small-only" />
                        </Link>
                    </li>
                </ul>
            </nav>
        </div>
    </header>
);

HeaderLoggedOut.defaultProps = {
    style: {},
};

HeaderLoggedOut.propTypes = {
    style: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.string,
    ]),
};


export default HeaderLoggedOut;

