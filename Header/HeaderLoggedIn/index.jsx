import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import uuid from 'uuid';
import HeaderLoggedIn from './HeaderLoggedIn';

const mapDispatchToProps = dispatch => ({
    toggleSearchBar: () => dispatch({ type: 'TOGGLE_SEARCH_BAR' }),
    focusOnSearchBar: () => dispatch({ type: 'FOCUS_ON_SEARCH_BAR', value: uuid() }),
});

export default connect(null, mapDispatchToProps)(withRouter(HeaderLoggedIn));
