import React from 'react';
import { shallow } from 'enzyme';
import { Link } from 'react-router';

import HeaderLoggedIn from './HeaderLoggedIn';

const routerMock = {
    push: jest.fn(),
    getCurrentLocation: () => ({ pathname: '/any-page' }),
};

const eventMock = {
    preventDefault: jest.fn(),
};

const toggleSearchBarAction = jest.fn();
const focusOnSearchBarAction = jest.fn();

describe('<HeaderLoggedIn /> component', () => {
    let component;

    beforeEach(() => {
        component = shallow(
            <HeaderLoggedIn
              router={routerMock}
              toggleSearchBar={toggleSearchBarAction}
              focusOnSearchBar={focusOnSearchBarAction}
            />,
        );
    });

    test('should display Twig logo', () => {
        const logo = component.find(Link).at(0);
        expect(logo.prop('to')).toEqual('/explore');
        expect(logo.dive().text()).toEqual('Twig Science Tools');
    });

    test('should display home link', () => {
        const home = component.find(Link).at(1);
        expect(home.prop('to')).toEqual('/explore');
        expect(home.dive().text()).toEqual('Home');
    });

    test('should display search link', () => {
        const search = component.find('a').at(0);
        expect(search.text()).toEqual('Search');
    });

    describe('after clicking the search link', () => {
        let searchLink;

        beforeEach(() => {
            jest.clearAllMocks();
            searchLink = component.find('a').at(0);
            searchLink.simulate('click', eventMock);
        });

        test('should invoke toggleSearchBar action', () => {
            expect(eventMock.preventDefault).toHaveBeenCalledTimes(1);
            expect(toggleSearchBarAction).toHaveBeenCalledTimes(1);
            expect(focusOnSearchBarAction).toHaveBeenCalledTimes(0);
        });
    });
});

describe('<HeaderLoggedIn /> component on the Search page', () => {
    describe('after clicking the search link', () => {
        test('should invoke focusOnSearch action', () => {
            jest.clearAllMocks();
            const routerMockOnSearch = {
                getCurrentLocation: () => ({ pathname: '/search' }),
            };

            const componentOnSearch = shallow(
                <HeaderLoggedIn
                  router={routerMockOnSearch}
                  toggleSearchBar={toggleSearchBarAction}
                  focusOnSearchBar={focusOnSearchBarAction}
                />);

            const searchLink = componentOnSearch.find('a').at(0);

            searchLink.simulate('click', eventMock);
            expect(eventMock.preventDefault).toHaveBeenCalledTimes(1);
            expect(focusOnSearchBarAction).toHaveBeenCalledTimes(1);
            expect(toggleSearchBarAction).toHaveBeenCalledTimes(0);
        });
    });
});


describe('<HeaderLoggedIn /> component on the Home page', () => {
    describe('after clicking the search link', () => {
        test('should invoke focusOnSearch action', () => {
            jest.clearAllMocks();
            const routerMockOnSearch = {
                getCurrentLocation: () => ({ pathname: '/explore' }),
            };

            const componentOnSearch = shallow(
                <HeaderLoggedIn
                  router={routerMockOnSearch}
                  toggleSearchBar={toggleSearchBarAction}
                  focusOnSearchBar={focusOnSearchBarAction}
                />);

            const searchLink = componentOnSearch.find('a').at(0);

            searchLink.simulate('click', eventMock);
            expect(eventMock.preventDefault).toHaveBeenCalledTimes(1);
            expect(focusOnSearchBarAction).toHaveBeenCalledTimes(1);
            expect(toggleSearchBarAction).toHaveBeenCalledTimes(0);
        });
    });
});


describe('<HeaderLoggedIn /> component on index page', () => {
    describe('after clicking the search link', () => {
        test('should invoke focusOnSearch action', () => {
            jest.clearAllMocks();
            const routerMockOnHomepage = {
                getCurrentLocation: () => ({ pathname: '/explore' }),
            };

            const componentOnSearch = shallow(
                <HeaderLoggedIn
                  router={routerMockOnHomepage}
                  toggleSearchBar={toggleSearchBarAction}
                  focusOnSearchBar={focusOnSearchBarAction}
                />);

            const searchLink = componentOnSearch.find('a').at(0);

            searchLink.simulate('click', eventMock);
            expect(eventMock.preventDefault).toHaveBeenCalledTimes(1);
            expect(focusOnSearchBarAction).toHaveBeenCalledTimes(1);
            expect(toggleSearchBarAction).toHaveBeenCalledTimes(0);
        });
    });
});
