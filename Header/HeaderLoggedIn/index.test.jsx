import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import HeaderLoggedInContainer from '.';

jest.mock('./HeaderLoggedIn');

const mockStore = configureMockStore();
const store = mockStore({});

describe('<HeaderLoggedIn> HOC Container', () => {
    test('should attach correct actions to the component', () => {
        const component = mount(
            <Provider store={store}>
                <HeaderLoggedInContainer />
            </Provider>);

        const props = component.find('HeaderLoggedIn').props();

        expect(props.toggleSearchBar()).toEqual({ type: 'TOGGLE_SEARCH_BAR' });
        expect(props.focusOnSearchBar()).toHaveProperty('type', 'FOCUS_ON_SEARCH_BAR');
        expect(props.focusOnSearchBar()).toHaveProperty('value');
    });
});
