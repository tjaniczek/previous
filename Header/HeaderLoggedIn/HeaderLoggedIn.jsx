import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import ExploreUrl from '../../../apps/Explore/url';
import SeachUrl from '../../../apps/Search/url';

require('./scss/HeaderLoggedIn.scss');

class HeaderLoggedIn extends React.Component {
    constructor(props) {
        super(props);

        this.onSearchClick = this.onSearchClick.bind(this);
    }

    onSearchClick(event) {
        event.preventDefault();
        const location = this.props.router.getCurrentLocation();

        if (this.onHomeOrSearch(location.pathname)) {
            this.props.focusOnSearchBar();
        } else {
            this.props.toggleSearchBar();
        }
    }

    onHomeOrSearch(path) {
        return path === '/' || path.startsWith(ExploreUrl) || path.startsWith(SeachUrl);
    }

    render() {
        return (
            <header className="site-header">
                <div className="site-header__container">
                    <div className="site-header__logo-wrapper">
                        <Link to={ExploreUrl} className="site-header__logo">
                            <h1 className="show-for-sr">Twig Science Tools</h1>
                        </Link>
                    </div>
                    <nav className="hide-for-print site-header__main-menu" role="navigation">
                        <ul>
                            <li className="hide-for-small-only">
                                <Link to={ExploreUrl}><span className="icon icon-home" />Home</Link>
                            </li>
                            <li><a href="" onClick={this.onSearchClick}>
                                <span className="icon icon-magnifying-glass" />
                                <span className="hide-for-small-only">Search</span>
                            </a></li>
                        </ul>
                    </nav>
                </div>
            </header>
        );
    }
}

HeaderLoggedIn.propTypes = {
    router: PropTypes.shape().isRequired,
    toggleSearchBar: PropTypes.func.isRequired,
    focusOnSearchBar: PropTypes.func.isRequired,
};

export default HeaderLoggedIn;
