

# Feature toggles

Feature toggles are controlled by a config file located in `features.js` . This file should follow structure:

```
export default {
    'test-feature': { development: false, staging: false, qa: false, production: false },
    'other-feature': { development: false, staging: false, qa: false, production: false },
};
```

- Flags for each feature determine if the feature should be visible in given environment.
- Features can be triggered for each environment independently.
- Review apps inherit `staging` enviroments.

When a feature is ready for QA the flag for `qa` should be set to `true`. After it has passed QA the `production` flag can be set to `true` if it is ready to be released.  If a feature does not pass QA it is possible for a release to progress after the feature switch has been turned off for QA and the site has been tested to ensure the feature switch doesn't break any existing features.

## Adding a toggle to the component definition

To toggle the component definition, wrap it in `featureToggle` HOC:

```
import featureToggle from '../../helpers/featureToggle';

class Feature extends React.Component {
    render() {
        return <div>My feature</div>;
    }
}

export default featureToggle('test-feature')(Feature); 
```

For stateless components:

```
import featureToggle from '../../helpers/featureToggle';

const Feature = () => <div>My feature</div>;
export default featureToggle('test-feature')(Feature);
```

## Adding a toggle to a component

Component may be hidden during the `render()` phase of other React component, like so:

```
render() {
    return (<div>
        <AvailableFeature />
        {featureToggle('work-in-progress')(<InDevelopment />)}
    </div>);
}
```

This method allows us to hide strings and HTML elements as well. Please note that `featureToggle` is curried, so it's possible to create one toggle and use it in multiple places:

```
import featureToggle from '../../helpers/featureToggle';

const inDevelopment = featureToggle('work-in-progress');

const Page = () => (<div>
    {inDevelopment(<h1>Page</h1>)}
    <AvailableFeature />
    {inDevelopment('And the other feature is:')}
    {inDevelopment(<InDevelopment />)}
</div>);
```

## Conditional JavaScript

`featureToggle` module also exposes a helper for conditionals in app's logic:

```
import { isEnabled } from '../../helpers/featureToggle';

if (isEnabled('this-feature')) {
   console.log('This feature is working!');
}
```

## Placeholders

On development/staging/qa environments a disabled component displays a placeholder ("component is under development"). This behaviour is **DISABLED** on production environment.

You can disable this behaviour for all environments by passing `false` to the HOC:

`export default featureToggle('test-feature', false)(Feature);`

## Fragments

There is no easy way to manage GraphQL fragments around conditional features, therefore using fragments always require extra attention. As a safety feature JavaScript will throw an error anytime you try to use a GQL fragment of a disabled component.

The best way to work with fragments is using `isEnabled` helper:

```
const productionQuery = gql`
query Lesson($id: String!) {
    lesson(id: $id {
        id
    }
}`;

const queryWithFeature = gql`
query Lesson($id: String!) {
    lesson(id: $id {
        id
            ...MyFeaturesFragment
    }
    ${MyFeature.fragments.fragment}
}`;

const query = isEnabled('my-feature') ? queryWithFeature : productionQuery;
```

Although managing queries for page with multiple conditional features may be extremely complecated and should be avoided as much as possible.
